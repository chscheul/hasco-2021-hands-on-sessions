#!/bin/bash
# Variables needed for the kernel building. DO NOT TOUCH!
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
ENV_FILE="environment.yml"
ENV_NAME="ROOT_conda_base"
ORIGIN_ENV="${SCRIPT_DIR}/${ENV_FILE}"

echo "######################################################"
echo "#          DOWNLOADING REQUIRED NTUPLES...           #"
echo "######################################################"
echo ""
# Downloads for ROOT Hands-on Session
mkdir -p ~/data/root_session/
echo "Downloading 'DataEgamma.root' into '~/data/root_session/'..."
curl -o ~/data/root_session/DataEgamma.root "https://owncloud.gwdg.de/index.php/s/D9ZVuGiTeU7K7j7/download?path=%2Froot_session&files=DataEgamma.root"
echo "Downloading 'DataMuons.root' into '~/data/root_session/'..."
curl -o ~/data/root_session/DataMuons.root "https://owncloud.gwdg.de/index.php/s/D9ZVuGiTeU7K7j7/download?path=%2Froot_session&files=DataMuons.root"
echo "Downloading 'mc_117049.ttbar_had.root' into '~/data/root_session/'..."
curl -o ~/data/root_session/mc_117049.ttbar_had.root "https://owncloud.gwdg.de/index.php/s/D9ZVuGiTeU7K7j7/download?path=%2Froot_session&files=mc_117049.ttbar_had.root"
echo "Downloading 'mc_117050.ttbar_lep.root' into '~/data/root_session/'..."
curl -o ~/data/root_session/mc_117050.ttbar_lep.root "https://owncloud.gwdg.de/index.php/s/D9ZVuGiTeU7K7j7/download?path=%2Froot_session&files=mc_117050.ttbar_lep.root" 
echo "Downloading 'mc_147771.Zmumu.root' into '~/data/root_session/'..."
curl -o ~/data/root_session/mc_147771.Zmumu.root "https://owncloud.gwdg.de/index.php/s/D9ZVuGiTeU7K7j7/download?path=%2Froot_session&files=mc_147771.Zmumu.root"
echo "Listing available files in '~/data/root_session/':"
ls -lh ~/data/root_session/

# Downloads for ML Hands-on Session
mkdir -p ~/data/ml_session/
echo "Downloading 'challenge.root' into '~/data/ml_session/'..."
curl -o ~/data/ml_session/challenge.root "https://owncloud.gwdg.de/index.php/s/D9ZVuGiTeU7K7j7/download?path=%2Fml_session&files=challenge.root"
echo "Downloading 'higgs.root' into '~/data/ml_session/'..."
curl -o ~/data/ml_session/higgs.root "https://owncloud.gwdg.de/index.php/s/D9ZVuGiTeU7K7j7/download?path=%2Fml_session&files=higgs.root"
echo "Downloading 'ttbar.root' into '~/data/ml_session/'..."
curl -o ~/data/ml_session/ttbar.root "https://owncloud.gwdg.de/index.php/s/D9ZVuGiTeU7K7j7/download?path=%2Fml_session&files=ttbar.root"
ls -lh ~/data/ml_session/

echo ""
echo "######################################################"
echo "#              DOWNLOAD PHASE COMPLETE!              #"
echo "######################################################"
echo -e "\n"
echo "######################################################"
echo "#            COMMENCING KERNEL BUILDING...           #"
echo "######################################################"
echo ""

. /opt/conda/etc/profile.d/conda.sh
echo "Creating Environment ${ENV_NAME}..."
conda env create -f ${ORIGIN_ENV} -p ~/${ENV_NAME}
echo "Activating Environment..."
conda activate ~/${ENV_NAME}
echo "Generating Kernel..."
python3 -m ipykernel install --user --name ${ENV_NAME} --display-name "Python (ROOT)"
echo "Available Kernels:"
conda deactivate
jupyter kernelspec list

echo -e "\n\n\n\n"
echo "######################################################"
echo "#                  KERNEL FINISHED!                  #"
echo "######################################################"
echo "# Please check whether there is a kernel called      #"
echo "# 'root_conda_base' available in the list above.  If #"
echo "# not, give one of the helpers a shout before you    #"
echo "# continue!                                          #"
echo "#                                                    #"
echo "# IF EVERYTHING WORKED:                              #"
echo "# ---------------------                              #"
echo "# Please consult the docs on how to restart your     #"
echo "# Jupyter server so that you can use the kernel for  #"
echo "# fun and profit!                                    #"
echo "######################################################"
