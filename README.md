# Welcome to HASCO!
In the following, we will set up your `JupyterHub` instance on GWDG. This will allow you to follow the hands-on tutorials with the very intuitive to use Jupyter notebooks. At the same time, it will also allow you to play around with `cling`, the C++ interpreter coming with the `ROOT` data analysis framework used extensively in particle physics.

But first things first... Let's get you set up!

[[_TOC_]]

## In case of problems
We have tried to make the set-up of the software needed for the hands-on sessions as easy to use as possible. Nevertheless, if you ever feel stuck in the course of this set-up session, please don't panic! Just ask one of our friendly helpers - that's what they are here for!

![XKCD_722](https://imgs.xkcd.com/comics/computer_problems.png)<br>[XKCD_722](https://xkcd.com/722/)

# Step-by-Step Instructions

## Logging into JupyterHub
For the entire set-up (and for the hands-on sessions themselves), we will use a `JupyterHub` instance hosted by GWDG. The first thing you should therefore do is log right in there and get to know the place!

For this, please do the following:
1. Open [https://jupyter-cloud.gwdg.de/](https://jupyter-cloud.gwdg.de/) in a new tab and log in there with your new shiny Uni Göttingen account
2. Look around! You should now be able to start various programs either as notebooks or from console. We will only be needing `python` and the `Terminal` for the set-up and hands-on sessions.
3. Open a new terminal (check out the picture below on how exactly to do that). This should now open a new tab for you, in which you have access to a normal `bash`-terminal. This will be needed for most of the set-up, while in the practical sessions, you will mostly use `python` notebooks (more on those later).

![Opening a Terminal in JupyterHub](https://gitlab.cern.ch/chscheul/hasco-2021-pictures/-/raw/master/Select_Console.png)

## Cloning the Repository (aka the codebase you are currently looking at)
First, you should download (or `clone`, in the GitLab lingo) this repository holding all the set-up scripts, configuration files, and the notebooks for testing and the hands-on sessions. To get all that good stuff, please follow these steps:

1. Type `pwd`. This will return the working directory to you (in fact, pwd stands for 'print working directory' - but people using command-line interfaces are lazy and have abbreviated it...). You should currently be in the directory '/home/jovyan/', which is your home directory (also abbreviated as `~` or `$HOME`).
2. Type `ls` - short for 'list' (laziness strikes again). This will print you an overview of everything currently living in your home directory.
3. Type `git clone https://gitlab.cern.ch/chscheul/hasco-2021-hands-on-sessions.git ~/HASCO_tutorial`. Here, `git` is a version control software that we used to set all the code up for you. At the same time `clone` tells git to get the repository you are currently looking at (yup, that's the one hiding behind that URL) into the folder `HASCO_tutorial` in your home directory.

Congratulations! Now you are the proud owner of your own git repository containing all the data you will need for the hands-on sessions. Next, we will see how you can actually use this to set-up your `python`-notebooks to be able to use `ROOT`.

## Downloading Samples and Creating Python Environment
Now that you have cloned the git repository containing the notebooks needed for the hands-on sessions and the set-up scripts, we will get right on making a nice python environment for you.
**Note:** This will only be needed for the `ROOT`-session, the ML-session works with a pre-configured `python` kernel.

For running the set-up script, simply type `source ~/HASCO_tutorial/setup/setup.sh`. File download and kernel building should now run fully automated (but might take a while). Please note the instructions printed to console at the end of the script!

## Restarting Server and Testing ROOT Kernel
**BEFORE YOU CONTINUE:**

Check that

 - A folder `data` with subfolders `ml_session` and `root_session` has been created in your home directory. The subfolders should contain 5 and 3 `.root`-files, respectively.
 - A kernel called `root_conda_base` was listed at the end of the set-up script.

**IF NOT:** Please contact one of our friendly helpers.

If everything seems to have gone great with the script (we hope so, it should be very easy to use), you now need to restart your Jupyter server to make your new kernel useable.

For this, follow the pictures below:

1. Access the server controls via `File > Hub Control Panel`
   ![Access Server Controls](https://gitlab.cern.ch/chscheul/hasco-2021-pictures/-/raw/master/Access_Server_Controls.png)

2. Shutdown server via `Stop My Server` (You might have to press this button twice)
   ![Shutdown Server](https://gitlab.cern.ch/chscheul/hasco-2021-pictures/-/raw/master/Shutdown_Server.png)

3. Restart server by pressing `Start My Server`
   ![Restart Server](https://gitlab.cern.ch/chscheul/hasco-2021-pictures/-/raw/master/Restart_Server.png)

4. Check that a new kernel named `Python (ROOT)` is available
   ![Check New Kernel](https://gitlab.cern.ch/chscheul/hasco-2021-pictures/-/raw/master/New_Kernel_Type.png)

5. Test new kernel by starting the notebook `ROOT_test.ipynb` in the folder `~/HASCO_tutorial/setup/` (please navigate there by using the menu on the left). The output of running the notebook should be similar to the one shown in the picture. Make sure that you run with the new `Python (ROOT)` kernel shown on the top right!
   ![Run Test Notebook](https://gitlab.cern.ch/chscheul/hasco-2021-pictures/-/raw/master/ROOT_Test_Notebook.png)

If everything worked correctly, you should have no problems running the test workbook. Otherwise, please ask one of our friendly helpers - I mean, just look at them, they are just waiting for something to go wrong! If everything went well, feel free to join the social events on [Wonder.me](https://www.wonder.me/r?id=b3c35740-540e-4be2-85bf-57eed534e9c2) and have fun during the school, especially the hands-on sessions! 

# For the impatient
If you already know your way around the concepts above and do not want to read very much, here is the TL;DR:

1. Sign into JupyterHub [https://jupyter-cloud.gwdg.de/](https://jupyter-cloud.gwdg.de/) and open a new terminal.
2. `git clone https://gitlab.cern.ch/chscheul/hasco-2021-hands-on-sessions.git ~/HASCO_tutorial`
3. `source ~/HASCO_tutorial/setup/setup.sh` (this step may take a while)
4. Restart your Jupyter server by shutting it down via `File > Hub Control Panel > Stop My Server` (the big red button, you might have to press it twice) and then restarting it via `Start My Server`
5. Test the new kernel by running `~/HASCO_tutorial/setup/ROOT_test.ipynb` with the new `Python (ROOT)` kernel.

If everything worked right, you should now be able to start a python notebook with a `Python (ROOT)` kernel and the `ROOT_test.ipynb`-notebook should work without any problem.

The modified kernel is only needed for the `ROOT` hands-on session. Additionally, there should be a folder `data` with subfolders `ml_session` and `root_session` in your home directory.

# Alternative: CERN SWAN

If you have a working CERN NICE Account, you can also use the notebooks on the [CERN SWAN service](https://swan.cern.ch/). The steps for this are as follows:

1. Click the button below to import the exercises into SWAN\
   [<img class="open_in_swan" data-path="basic" alt="Open these notebooks in SWAN" src="https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png">][repo_url]

   [repo_url]:https://cern.ch/swanserver/cgi-bin/go?projurl=https://gitlab.cern.ch/chscheul/hasco-2021-root-update.git
2. Start a session in SWAN with the default parameters.
3. Run the `copy_swan.ipynb` notebook to load the ntuples (into `/eos/user/<YOUR USER>/SWAN_projects/hasco-2021-root-update`)

Now, you can run the notebooks on SWAN (please change the `on_swan` variable in each first cell to `True` for this).

# Update for ROOT-Session (GWDG ONLY, not required for SWAN!)

For the ROOT hands-on session, you will still need to get some additional samples and the required notebooks. Please follow these steps to obtain them:

1. Open a terminal in GWDG's JupyterHub
2. Type `git clone https://gitlab.cern.ch/chscheul/hasco-2021-root-update.git ~/HASCO_ROOT_Session`
3. Download additional ntuples: `source ~/HASCO_ROOT_Session/update_ntuples.sh`

Now you should be able to access the ROOT-Session Notebooks in the `~/HASCO_ROOT_Session` folder.

# Known Issues

It may be that you encounter a problem like shown in the picture below when you try to run the test notebook.

![Jupyter Notebook No-ROOT-Module Error](https://gitlab.cern.ch/chscheul/hasco-2021-pictures/-/raw/master/No_Root_Module.png)

This is likely caused by some corrupted packages - and unfortunately did not happen to our testers...

However, the problem can be fixed as follows. Please open a new terminal for this.

1. First, you will have to delete the kernel... It was no good anyway:
   ```
   jupyter kernelspec remove root_conda_base
   rm -rfvd ~/ROOT_conda_base
   ```

2. Now, you need to find out which packages are troublesome. Run the following for this:

   ```
   . /opt/conda/etc/profile.d/conda.sh
   # This step may take some time unfortunately...
   conda create -y -c conda-forge --prefix ~/ROOT_conda_base root_base 2>&1 | tee ~/log.txt
   ```

   The offending packages should now generate errors at the end of the logfile, like this:

   ```
   CondaVerificationError: The package for libedit located at /home/jovyan/.conda/pkgs/libedit-3.1.20191231-he28a2e2_2
   appears to be corrupted. The path 'share/man/man3/editline.3' specified in the package manifest cannot be found.

   CondaVerificationError: The package for libedit located at /home/jovyan/.conda/pkgs/libedit-3.1.20191231-he28a2e2_2
   appears to be corrupted. The path 'share/man/man3/el_end.3' specified in the package manifest cannot be found.
   ```

   You can print the logfile to console like this: `cat ~/log.txt`

   There might be several of these errors. The important part of them is the path to the package, in our example `/home/jovyan/.conda/pkgs/libedit-3.1.20191231-he28a2e2_2`. Please collect all different ones of these present in the errors (there might be multiple, and multiple errors with the same path!), you will need them in the next step.

   If you cannot see any errors in the log, skip directly to step 6.

3. Delete the generated conda environment again: `rm -rfvd ~/ROOT_conda_base`.
4. Remove the offending packages. For this, please remove everything at the paths you just collected. For example:
   
   ```
   rm -rfvd /home/jovyan/.conda/pkgs/libedit-3.1.20191231-he28a2e2_2
   ```
5. Now you can remake the conda environment like so:

   ```
   . /opt/conda/etc/profile.d/conda.sh
   # This first step may take some time unfortunately...
   conda create -y -c conda-forge --prefix ~/ROOT_conda_base root_base
   ```

6. Lastly, create a kernel from the environment:

   ```
   conda activate root_conda_base
   python3 -m pip install jupyter
   python3 -m ipykernel install --user --name ROOT_conda_base --display-name "Python (ROOT)"
   conda deactivate
   ```
   
   If you close your terminal at any point during this, please rerun `. /opt/conda/etc/profile.d/conda.sh` first.

   You will also need to activate the conda environment before you execute the two `python3`-commands. Make sure you can see `(/home/jovyan/ROOT_conda_base)` at the beginning of the command prompt during these steps, then you are in the correct conda environment.

Afterwards, you can again restart the server and try out the testing notebook.

